'use strict';

const chalk = require('chalk');

class CachedGenerator {
    initializing(configuration) {
        console.log(chalk.red(`Initializing`));
        this.configuration = configuration;
        console.log(this.configuration.yeoman.sourceRoot());
        console.log(this.configuration.yeoman.destinationPath());
        console.log(this.configuration.yeoman.templatePath());
    }

    prompting() {
        console.log(chalk.red(`Prompting`));
    }

    configuring() {
        console.log(chalk.red(`Configuring`));
    }

    default() {
        this._method1();
        this._method2();
    }

    _method1() {
        console.log(chalk.red(`Default 1`));
    }

    _method2() {
        console.log(chalk.red(`Default 2`));
    }

    writing() {
        console.log(chalk.red(`Writing`));
        this.configuration.yeoman.fs.copy(
            this.configuration.yeoman.templatePath('hello.world.html'),
            this.configuration.yeoman.destinationPath('public/hello.world.html'),
        );
    }

    conflicts() {
        console.log(chalk.red(`Conflicts`));
    }

    install() {
        console.log(chalk.red(`Install`));
    }

    end() {
        console.log(chalk.red(`End`));
    }
};

module.exports = new CachedGenerator();