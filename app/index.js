'use strict';

const Generator = require('yeoman-generator');
const path = require('path');

const CONSTANTS = require('./../utils/constants');
const downloader = require('./../utils/downloader');

class AppGenerator extends Generator {
    constructor(args, opts) {
        super(args, opts);
        this.generatorConfiguration = {
            args,
            name: opts.namespace.slice(CONSTANTS.GENERATOR_NAME.length + 1),
            opts,
            yeoman: this
        };

        this.downloaded = false;
        this.generator;
    }

    async initializing() {
        const generatorName = this.generatorConfiguration.name;

        // 1. Attempt to download the generator template
        this.downloaded = await downloader.download(generatorName, this.sourceRoot());

        // 2. Allocate either the cached generator or the downloaded one, clear require cache to avoid issues with
        //      possible updates on the downloaded generator
        let generatorPath;
        if (this.downloaded) {
            generatorPath = CONSTANTS.GENERATOR_WORKSPACE;
        } else {
            generatorPath = CONSTANTS.GENERATOR_CACHE.replace('{generator}', generatorName);
            // Change the source root to the generator cache folder
            const templatesCacheFolder = this.sourceRoot().replace(
                path.join(generatorName, 'templates'),
                path.join('cache', generatorName, 'templates'));
            this.sourceRoot(templatesCacheFolder);
        }

        delete require.cache[require.resolve(generatorPath)];
        this.generator = require(generatorPath);

        // 3. Invoke generator's initializing
        this.generator.initializing(this.generatorConfiguration);
    }

    prompting() {
        this.generator.prompting();
    }

    configuring() {
        this.generator.configuring();
    }

    writing() {
        this.generator.writing();
    }

    default() {
        this.generator.default();
    }

    conflicts() {
        this.generator.conflicts();
    }

    install() {
        this.generator.install();
    }

    async end() {
        this.generator.end();
        // The sourceRoot folder should only be cleared if the content has been downloaded
        if (this.downloaded) {
            await downloader.clearFolder(this.sourceRoot());
        }
    }
};

module.exports = AppGenerator;