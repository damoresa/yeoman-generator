'use strict';

const CONSTANTS = {
    BITBUCKET: {
        BRANCH: 'get/master.zip',
        HOST: 'https://bitbucket.org/damoresa/',
        app: 'yeoman-template/'
    },
    GENERATOR_CACHE: './../cache/{generator}/templates/cached.generator',
    GENERATOR_NAME: 'test',
    GENERATOR_WORKSPACE: './templates/generator'
};

module.exports = CONSTANTS;