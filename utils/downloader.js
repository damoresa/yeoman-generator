'use strict';

const Promise = require('bluebird');
const extract = require('extract-zip');
const fse = require('fs-extra');
const path = require('path');
const request = require('request-promise');

// Use bluebird to promisify the non-promise libraries.
// EXTREMELY USEFUL
const extractor = Promise.promisify(extract);
const readdir = Promise.promisify(fse.readdir);
const stat = Promise.promisify(fse.stat);

const CONSTANTS = require('./../utils/constants');

class Downloader {
    async clearFolder(folder) {
        console.log(`Clearing ${folder}`);
        await fse.remove(folder);
    }

    async download(generator, folder) {
        try {
            await this.clearFolder(folder);
            const repositoryUrl =
                `${CONSTANTS.BITBUCKET.HOST}${CONSTANTS.BITBUCKET[generator]}${CONSTANTS.BITBUCKET.BRANCH}`;
            const targetFolder = folder.replace('templates', '');
            const targetName = `${targetFolder}templates.zip`;

            console.log(`Downloading ${repositoryUrl} on ${targetName}`);
            const requestOpts = {
                url: repositoryUrl,
                encoding: null
            };

            const data = await request.get(requestOpts);
            await fse.writeFileSync(targetName, data);

            console.log(`Extracting the downloaded assets`);
            await extractor(targetName, { dir: targetFolder });

            const extractedFolder = await this._findExtractedFolder(targetFolder);
            await fse.move(path.join(targetFolder, extractedFolder, 'templates'), path.join(targetFolder, 'templates'));

            console.log(`Clearing previously downloaded assets`);
            await fse.remove(targetName);
            await fse.remove(path.join(targetFolder, extractedFolder));

            return true;
        } catch (err) {
            console.error(err);
            return false;
        }
    }

    async _findExtractedFolder(folder) {
        const folderContent = await readdir(folder);

        let extractedFolder;
        let contentIdx = 0;
        while(!extractedFolder && contentIdx < folderContent.length) {
            const contentData = await stat(path.join(folder, folderContent[contentIdx]));
            if (contentData.isDirectory()) {
                extractedFolder = folderContent[contentIdx];
            } else {
                contentIdx++
            }
        }
        return extractedFolder;
    }
};

module.exports = new Downloader();