## Yeoman generator with remote templates

This repository features a _POC_ of a _Yeoman_ generator which fetches it's templates 
from external repositories, such as 
[this](https://bitbucket.org/damoresa/yeoman-template/src/master/).

This is extremely helpful for generators as they can leverage all the template updating 
to online processes which don't force the user to update the generator version, as the 
generator itself is just a shell that executes the real generator code.